# BIDSError

::: psychopy_bids.bids.bidserror.BIDSError
    selection:
      docstring_style: numpy

# OnsetError

::: psychopy_bids.bids.bidserror.OnsetError
    selection:
      docstring_style: numpy

# DurationError

::: psychopy_bids.bids.bidserror.DurationError
    selection:
      docstring_style: numpy

# TrialTypeError

::: psychopy_bids.bids.bidserror.TrialTypeError
    selection:
      docstring_style: numpy

# SampleError

::: psychopy_bids.bids.bidserror.SampleError
    selection:
      docstring_style: numpy

# ResponseTimeError

::: psychopy_bids.bids.bidserror.ResponseTimeError
    selection:
      docstring_style: numpy

# HedError

::: psychopy_bids.bids.bidserror.HedError
    selection:
      docstring_style: numpy

# StimFileError

::: psychopy_bids.bids.bidserror.StimFileError
    selection:
      docstring_style: numpy

# IdentifierError

::: psychopy_bids.bids.bidserror.IdentifierError
    selection:
      docstring_style: numpy

# DatabaseError

::: psychopy_bids.bids.bidserror.DatabaseError
    selection:
      docstring_style: numpy